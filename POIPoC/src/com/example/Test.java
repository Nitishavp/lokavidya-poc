package com.example;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.xslf.usermodel.SlideLayout;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFSlideLayout;
import org.apache.poi.xslf.usermodel.XSLFSlideMaster;
import org.apache.poi.xslf.usermodel.XSLFTextShape;

public class Test {

	public static void main(String[] args) throws IOException
	{
//		XMLSlideShow ppt = new XMLSlideShow();
//		File file=new File("/home/sanket/development/temp/example1.pptx");
//		FileOutputStream out = new FileOutputStream(file);
//		ppt.write(out);
		
		
		//creating presentation
	      XMLSlideShow ppt = new XMLSlideShow();	    	
	      
	      //getting the slide master object
	      XSLFSlideMaster slideMaster = ppt.getSlideMasters().get(0);
	      
	      //get the desired slide layout 
	      XSLFSlideLayout titleLayout = slideMaster.getLayout(SlideLayout.TITLE);
	                                                     
	      //creating a slide with title layout
	      XSLFSlide slide1 = ppt.createSlide(titleLayout);
	      
	      //selecting the place holder in it 
	      XSLFTextShape title1 = slide1.getPlaceholder(0); 
	      
	      //setting the title init 
	      title1.setText("Sasa point");
	      
	      //create a file object 
	      File file=new File("C:/Users/hp/Documents/Simpletrial.pptx");
	      FileOutputStream out = new FileOutputStream(file);
	      
	      //save the changes in a PPt document
	      ppt.write(out);
	      System.out.println("slide created successfully");
	      out.close();  
	      
	      
	      
	      
	      
	      
	            //creating presentation
	          ppt = new XMLSlideShow();
	            
	            //getting the slide master object
	            slideMaster = ppt.getSlideMasters().get(0);
	            
	            //select a layout from specified list
	            XSLFSlideLayout slidelayout = slideMaster.getLayout(SlideLayout.TITLE_AND_CONTENT);      
	            
	            //creating a slide with title and content layout
	            XSLFSlide slide = ppt.createSlide(slidelayout);
	            //selection of title place holder
	            XSLFTextShape title = slide.getPlaceholder(0);
	            
	            //setting the title in it
	            title.setText("introduction");
	            
	            //selection of body placeholder
	            XSLFTextShape body = slide.getPlaceholder(1);
	            
	            //clear the existing text in the slide
	            body.clearText();
	            
	            //adding new paragraph
	            body.addNewTextParagraph().addNewTextRun().setText("this is  my first slide body");
	            
	            //create a file object
	            file=new File("C:/Users/hp/Documents/Simpletrial2.pptx");
	            out = new FileOutputStream(file);
	            
	            //save the changes in a file
	            ppt.write(out);
	            System.out.println("slide created successfully");
	            out.close();                
	         
	            
	            
	                  
	                  //creating an empty presentation
	            	file=new File("C:/Users/hp/Documents/Simpletrial2.pptx");
	                 ppt = new XMLSlideShow(new FileInputStream(file));
	                  
	                  //getting the dimensions and size of the slide 
	                  Dimension pgsize = ppt.getPageSize();
	                  List<XSLFSlide> slides = ppt.getSlides();
	                  BufferedImage img = null;
	                  for (int i = 0; i < slides.size(); i++) {
	                     img = new BufferedImage(pgsize.width, pgsize.height,BufferedImage.TYPE_INT_RGB);
	                     Graphics2D graphics = img.createGraphics();

	                     //clear the drawing area
	                     graphics.setPaint(Color.white);
	                     graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));

	                     //render
	                     slides.get(i).draw(graphics);
	                     
	                  }
	                  
	                  //creating an image file as output 
	                  out = new FileOutputStream("crops.jpg");
	               javax.imageio.ImageIO.write(img, "jpg", out);
	                  ppt.write(out);
	                  
	                  System.out.println("Image successfully created");
	                  out.close();
	}
}


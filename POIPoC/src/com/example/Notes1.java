package com.example;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFNotes;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.apache.poi.xslf.usermodel.Placeholder;

public class Notes1 {
	public static void main(String args[])
	{
	try {

		FileInputStream fis = new FileInputStream("F:\\IITB\\Ocean currents.pptx");
	    XMLSlideShow pptxshow = new XMLSlideShow(fis);
	    fis.close();
	    
	    for(XSLFSlide slide : pptxshow.getSlides()) {
	    	XSLFNotes note = pptxshow.getNotesSlide(slide);

	        // insert text
	        for (XSLFTextShape shape : note.getPlaceholders()) {
	            if (shape.getTextType() == Placeholder.BODY) {
	                shape.setText("Nitisha Pandharpurkar");
	                break;
	            }
	        }

	    	
	    }
	} catch (IOException e) {

	}
}
}
package com.dialogs;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class TextArea extends JPanel implements ActionListener {
    protected JTextArea textArea;
    protected JButton button;
    private final static String newline = "\n";
    private JTextField textField;

    public TextArea() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWeights = new double[]{0.0, 1.0};
        
        SpringLayout springLayout = new SpringLayout();
        setLayout(springLayout);
        
        textField = new JTextField();
        springLayout.putConstraint(SpringLayout.NORTH, textField, 50, SpringLayout.NORTH, this);
        springLayout.putConstraint(SpringLayout.WEST, textField, 36, SpringLayout.WEST, this);
        springLayout.putConstraint(SpringLayout.EAST, textField, 193, SpringLayout.WEST, this);
        GridBagConstraints gbc_textField = new GridBagConstraints();
        gbc_textField.insets = new Insets(0, 0, 5, 0);
        gbc_textField.fill = GridBagConstraints.HORIZONTAL;
        gbc_textField.gridx = 1;
        gbc_textField.gridy = 0;
        add(textField);
        textField.setColumns(10);
        
        JTextArea textArea = new JTextArea();
        textArea.setEditable(false);
        springLayout.putConstraint(SpringLayout.NORTH, textArea, 6, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.WEST, textArea, 0, SpringLayout.WEST, textField);
        springLayout.putConstraint(SpringLayout.SOUTH, textArea, 191, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.EAST, textArea, 234, SpringLayout.WEST, this);
        add(textArea);
        
        JButton btnNewButton = new JButton("+");
        springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 0, SpringLayout.NORTH, textField);
        springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 6, SpringLayout.EAST, textField);
        springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, 0, SpringLayout.SOUTH, textField);
        springLayout.putConstraint(SpringLayout.EAST, btnNewButton, 0, SpringLayout.EAST, textArea);
        btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 7));
        //btnNewButton.addActionListener(new ActionListener());
        button.addActionListener(this);
        add(btnNewButton);
        
        //button=new JButton("+");
        //button.addActionListener(this);

        textArea = new JTextArea(5, 20);
        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        
        //c.fill=GridBagConstraints.HORIZONTAL;
        //c.gridx=1;
        //c.gridy=0;
        //add(button, c);
        
        //c.fill = GridBagConstraints.BOTH;
        //c.weightx = 1.0;
        //c.weighty = 1.0;
        //add(scrollPane, c);
    }
    public void actionPerformed(ActionEvent evt) {
        String text = textField.getText();
        textArea.append(text + newline);
        textField.selectAll();

        //Make sure the new text is visible, even if there
        //was a selection in the text area.
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
   
}    
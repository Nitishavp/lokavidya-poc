package in.iitb.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

enum DocumentType
{
	HEADINGS,PHRASES
};

class Element
{
	String s;
	boolean value;
}
public class GlobalTemplateInformation {

	ArrayList<Element> phrases,headings;
	
	public GlobalTemplateInformation(String currentDir) {
		load(currentDir);
	}

	private void load(String currentDir) {
		File headingsFile = new File(currentDir,DocumentType.HEADINGS.toString()+".json");
		File phrasesFile = new File(currentDir, DocumentType.PHRASES.toString()+".json");
		Gson gson = new Gson();
		try {

			BufferedReader br = new BufferedReader(new FileReader(headingsFile));
			Type listType = new TypeToken<ArrayList<Element>>() {}.getType();
			//convert the json string back to object
			headings = gson.fromJson(br, listType);
			
			br = new BufferedReader(new FileReader(phrasesFile));
			listType = new TypeToken<ArrayList<Element>>() {}.getType();
			//convert the json string back to object
			phrases = gson.fromJson(br, listType);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<String> getListofHeadings()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0;i<headings.size();i++)
		{
			temp.add(headings.get(i).s);
		}
		return temp;
	}
	
	public List<String> getListofRecommendedHeadings()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0;i<headings.size();i++)
		{
			if(headings.get(i).value==true)
			temp.add(headings.get(i).s);
		}
		return temp;
	}
	
	public List<String> getListofUnRecommendedHeadings()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0;i<headings.size();i++)
		{
			if(headings.get(i).value==false)
			temp.add(headings.get(i).s);
		}
		return temp;
	}
	
	public List<String> getListofPhrases()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0;i<phrases.size();i++)
		{
			temp.add(phrases.get(i).s);
		}
		return temp;
	}
		
	public List<String> getListofRecommendedPhrases()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0;i<phrases.size();i++)
		{
			if(phrases.get(i).value==true)
			temp.add(phrases.get(i).s);
		}
		return temp;
	}
	
	public List<String> getListofUnRecommendedPhrases()
	{
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0;i<phrases.size();i++)
		{
			if(phrases.get(i).value==false)
			temp.add(phrases.get(i).s);
		}
		return temp;
	}
	
}

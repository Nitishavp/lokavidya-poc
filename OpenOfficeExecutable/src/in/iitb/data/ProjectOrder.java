package in.iitb.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import in.iitb.data.atoms.Atom;

public class ProjectOrder extends Document {

	List<Atom> orderList;
	
	public ProjectOrder(Project project)
	{
		super(project, "projectmeta.json");
		parse();
	}
	
	@Override
	public void parse()
	{
		Gson gson = new Gson();
		File projectmeta = new File(this.getRootProject().getProjectLocalURL(),"projectmeta.json");
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(projectmeta));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Type listType = new TypeToken<ArrayList<Atom>>() {}.getType();
		//convert the json string back to object
		projectmeta = gson.fromJson(br, listType);
	}
	
	public void save()
	{
		
	}
	
}

package in.iitb.function;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class FFMPEGWrapper {

	String pathExecutable;
	
	public FFMPEGWrapper(String sourcePath)
	{
		String osname=System.getProperty("os.name");
		System.out.println(osname);
		if (osname=="Windows")
		{
			pathExecutable = new File(new File(new File(sourcePath,"ffmpegwindows").getAbsolutePath(),"bin"),"ffmpeg.exe").getAbsolutePath();
		}
		else
		{
			System.out.println("Setting to Linux");
			pathExecutable = new File(new File(sourcePath,"ffmpeglinux").getAbsolutePath(),"ffmpeg").getAbsolutePath();
			System.out.println(pathExecutable);
		}
	}
	
	public boolean stitchImageAndAudio(String imgPath, String audioPath, String videoPath,String tempPath) throws IOException, InterruptedException
	{
		/*
		 * Check if final or temp exists delete if it does.
		 */
		File temp=  new File(tempPath);
		File video = new File(videoPath);
		if (temp.exists())
			temp.delete();
		if (video.exists())
			video.delete();
		String cmd =" "+pathExecutable;//+" -help";
		String cmdext= " -loop 1 -i "+imgPath+" -c:v libx264 -t 2 -pix_fmt yuv420p -vf scale=320:240 "+tempPath;
		cmd +=cmdext;
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		cmd =" "+pathExecutable;//+" -help";
		cmdext=" -i "+audioPath+" -i "+tempPath+" -c:a copy -vcodec copy -strict -2 "+videoPath;
		cmd +=cmdext;
		run = Runtime.getRuntime();
		pr = run.exec(cmd);
		pr.waitFor();
		buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return true;
	}
	
	public boolean stitchVideo(List<String> videoPaths,String tempPath,String finalPath) throws IOException, InterruptedException
	{
		//Writing files to order.
        File orderVideoFile = new File(tempPath,"order.txt");
		try {
			FileOutputStream out = new FileOutputStream(orderVideoFile);
            for(int i=0;i<videoPaths.size();i++) {
                String data="file '"+videoPaths.get(i)+"'\n";
                out.write(data.getBytes());
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
		String cmd=" "+pathExecutable;//+" -help";
		String cmdext= " -f concat -i    "+orderVideoFile.getAbsolutePath()+" -codec copy  "+finalPath;
		cmd+=cmdext;
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line=buf.readLine())!=null) {
		System.out.println(line);
		}
		return false;
		
	}
}

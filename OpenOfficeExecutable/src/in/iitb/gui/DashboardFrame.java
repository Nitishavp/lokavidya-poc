package in.iitb.gui;
import com.alee.examples.WebLookAndFeelDemo;
import com.alee.examples.content.DefaultExample;
import com.alee.extended.panel.GroupPanel;
import com.alee.extended.panel.GroupingType;
import com.alee.extended.window.TestFrame;
import com.alee.laf.button.WebButton;
import com.alee.demo.style.WebLafDemoSkin;
import com.alee.laf.WebLookAndFeel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.toolbar.WhiteSpace;
import com.alee.managers.language.LanguageManager;
import com.alee.managers.settings.SettingsManager;
import com.alee.managers.style.StyleManager;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;



class MyPanel2 extends JPanel
{
	private JTextField How;
	private JLabel jcomp2;
	private JLabel jcomp3;
	private JButton jcomp4;
	public MyPanel2()
	{
		//construct components
		   How = new JTextField (1);
		   jcomp2 = new JLabel ("How long were you parked?");
		   jcomp3 = new JLabel ("Minutes");
		   jcomp4 = new JButton ("openNewWindow");
		
		   jcomp4.addActionListener(new ActionListener() {
		       @Override
		       public void actionPerformed(ActionEvent e) {
		           JFrame frame = new JFrame ("MyPanel");
		           frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		           frame.getContentPane().add (new MyPanel2());
		           frame.pack();
		           frame.setVisible (true);
		
		       }
		   });
		
		   //adjust size and set layout
		   setPreferredSize (new Dimension (315, 85));
		   setLayout (null);
		
		   //add components
		   add (How);
		   add (jcomp2);
		   add (jcomp3);
		   add (jcomp4);
		
		   //set component bounds (only needed by Absolute Positioning)
		   How.setBounds (245, 50, 60, 25);
		   jcomp2.setBounds (35, 30, 185, 50);
		   jcomp3.setBounds (250, 30, 60, 20);
		   jcomp4.setBounds (0, 0, 315, 25);
		
		      jcomp4.addActionListener( new ActionListener()
		   {
		       public void actionPerformed(ActionEvent e)
		       {
		
		       }
		   });
	}
}
class MyPanel extends JPanel {
	private JTextField How;
	private JLabel jcomp2;
	private JLabel jcomp3;
	private JButton jcomp4;
	
	public MyPanel() {
	   //construct components
	   How = new JTextField (1);
	   jcomp2 = new JLabel ("How long were you parked?");
	   jcomp3 = new JLabel ("Minutes");
	   jcomp4 = new JButton ("openNewWindow");
	
	   jcomp4.addActionListener(new ActionListener() {
	       @Override
	       public void actionPerformed(ActionEvent e) {
	           JFrame frame = new JFrame ("MyPanel");
	           frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	           frame.getContentPane().add (new MyPanel2());
	           frame.pack();
	           frame.setVisible (true);
	
	       }
	   });
	
	   //adjust size and set layout
	   setPreferredSize (new Dimension (315, 85));
	   setLayout (null);
	
	   //add components
	   add (How);
	   add (jcomp2);
	   add (jcomp3);
	   add (jcomp4);
	
	   //set component bounds (only needed by Absolute Positioning)
	   How.setBounds (245, 50, 60, 25);
	   jcomp2.setBounds (35, 30, 185, 50);
	   jcomp3.setBounds (250, 30, 60, 20);
	   jcomp4.setBounds (0, 0, 315, 25);
	
	      jcomp4.addActionListener( new ActionListener()
	   {
	       public void actionPerformed(ActionEvent e)
	       {
	
	       }
	   });
	}
}
public class DashboardFrame extends DefaultExample {
	
		public DashboardFrame()
		{
			super();
		}
	
		public static void main(String[] args)
		{
			SwingUtilities.invokeLater ( new Runnable ()
	        {
	            public void run ()
	            {
	                // Initialize L&F here, before creating any UI
	            	WebLookAndFeel.install ();
	            	
	            	
	                final JTextArea textArea = new JTextArea ( "Simple text area" );
	                final JScrollPane scrollPane = new JScrollPane ( textArea );
	                scrollPane.setPreferredSize ( new Dimension ( 300, 150 ) );
	                scrollPane.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );
	                scrollPane.setHorizontalScrollBarPolicy ( ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS );

	                final JProgressBar progressBar = new JProgressBar ();
	                progressBar.setIndeterminate ( true );
	                final JButton create= new JButton("Create");
	                final JButton importButton=  new JButton("Import");
	                final JButton ok = new JButton ( "Ok" );
	                final JButton cancel = new JButton ( "Cancel" );
	                JFrame frame = new JFrame("Colored Trails");
	                
	                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	                JPanel mainPanel = new JPanel();
	                mainPanel.setLayout((LayoutManager) new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

	                JPanel firstPanel = new JPanel();
	                firstPanel.setLayout(new GridLayout(4, 4));
	                JButton btn;
	                
	                
	                for (int i=1; i<=4; i++) {
	                        btn = new JButton();
	                        btn.setPreferredSize(new Dimension(100, 100));
	                        firstPanel.add(btn);
	                        btn.addActionListener(new ActionListener() {
	                            @Override
	                            public void actionPerformed(ActionEvent e) {
	                                JFrame frame = new JFrame ("MyPanel");
	                                frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
	                                frame.getContentPane().add (new MyPanel2());
	                                frame.pack();
	                                frame.setVisible (true);

	                            }
	                        });
	                }

	                

	                mainPanel.add(firstPanel);
	                frame.add(mainPanel);

	                frame.setSize(400,600);
	                frame.setVisible(true);
	                
	                TestFrame.show ( new GroupPanel ( GroupingType.none,10, false,create,importButton,
	                	new DashboardFrame().getPreview(WebLookAndFeelDemo.getInstance()) ), 10 );
	            }
	        } );
			new DashboardFrame();
		}
	    @Override
	    public String getTitle ()
	    {
	        return "Buttons";
	    }

	    @Override
	    public String getDescription ()
	    {
	        return "Web-styled buttons";
	    }

	    @Override
	    public Component getPreview ( WebLookAndFeelDemo owner )
	    {
	        // Simple button
	        WebButton b = new WebButton ( "Simple" );

//	        // Disabled button
//	        WebButton db = new WebButton ( "Iconed", loadIcon ( "resources/icon.jpg" ) );

	        return new GroupPanel ( 2, new GroupPanel ( b ));
	    }
}

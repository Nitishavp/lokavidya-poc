package com.ext;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SpringLayout;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Start {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Start window = new Start();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Start() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 565, 329);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblWelcome = new JLabel("Welcome to the");
		lblWelcome.setForeground(Color.DARK_GRAY);
		lblWelcome.setFont(new Font("Tahoma", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.NORTH, lblWelcome, 26, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblWelcome, 32, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblWelcome, 549, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblWelcome);
		
		JLabel lblNewLabel = new JLabel("Video Workbench");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 17, SpringLayout.SOUTH, lblWelcome);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 92, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel, 431, SpringLayout.WEST, frame.getContentPane());
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("New Project");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frame.getContentPane().add(btnNewButton);
		
		JButton btnOpenProject = new JButton("Open Project");
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 0, SpringLayout.WEST, btnOpenProject);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -22, SpringLayout.NORTH, btnOpenProject);
		springLayout.putConstraint(SpringLayout.WEST, btnOpenProject, 73, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btnOpenProject, -38, SpringLayout.SOUTH, frame.getContentPane());
		btnOpenProject.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frame.getContentPane().add(btnOpenProject);
		
		JLabel lblNewLabel_1 = new JLabel("New to Video Workbench? ");
		lblNewLabel_1.setForeground(Color.DARK_GRAY);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_1, 0, SpringLayout.NORTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_1, 153, SpringLayout.EAST, btnNewButton);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblReadInstructions = new JLabel("Read instructions");
		springLayout.putConstraint(SpringLayout.NORTH, lblReadInstructions, 9, SpringLayout.SOUTH, lblNewLabel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblReadInstructions, 10, SpringLayout.WEST, lblNewLabel_1);
		frame.getContentPane().add(lblReadInstructions);
		
		JButton btnHere = new JButton("here");
		springLayout.putConstraint(SpringLayout.NORTH, btnHere, 0, SpringLayout.NORTH, lblReadInstructions);
		springLayout.putConstraint(SpringLayout.WEST, btnHere, 5, SpringLayout.EAST, lblReadInstructions);
		btnHere.setBorder(null);
		btnHere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		frame.getContentPane().add(btnHere);
	}
}
